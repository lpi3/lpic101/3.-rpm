# Certification LPIC 1 - 101-500

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser la commande **rpm**, permettant de manipuler les paquets des distributions RedHat et CentOS.

<br/>

---



# Exercice 1

Dans ce premier exercice, nous allons utiliser la commande `rpm` pour installer des paquets.

**RPM** est le principal outil permettant d'installer, de configurer et de désinstaller des paquets appartenant à la famille RedHat.

<br/>

## Commande générale et introduction

L'opération classique consistant à installer un paquet peut se faire grâce à la commande : `rpm -i PACKAGENAME`

Prenons par exemple le paquet : **cowsay-3.04-4.el7.noarch.rpm** qui permet d'afficher du texte à l'écran de manière sympathique.

<br/>

1. Téléchargeons le paquet grâce à la commande suivante : `wget https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/c/cowsay-3.04-4.el7.noarch.rpm`

<br/>

2. Maintenant, installez le paquet grâce à la commande **rpm**.

<details><summary> Montrer la solution </summary>
<p>
rpm -i cowsay-3.04-4.el7.noarch.rpm 
</p>
</details>

<br/>

Maintenant que vous avez exécuté la commande correspondante, répondez aux questions ci-dessous :

**Q1: Est-ce que le paquet a été correctement installé ?**

[  ] Oui

[  ] Non

<details><summary> Montrer la solution </summary>
<p>
Oui le paquet s'est correctement installé. Par de dépendances particulières pour cowsay.
</p>
</details>

<br/>

*Remarque: S'il existe une version antérieure du paquet sur le système, vous pouvez le mettre à jour grâce au paramètre -U de la commande rpm. Cependant, comme avec dpkg, si aucune version n’existe déjà, alors une nouvelle installation sera effectuée. Pour empêcher cela, et uniquement effectuer une mise à jour d’un paquet déjà installé, vous pouvez utiliser l’option -F.* (à savoir pour l'examen)

<br/>

## Obtenir plus d'informations sur l'installation du paquet

Comme vous avez pu le constater, la sortie n’est pas très « verbeuse », et si vous voulez avoir des informations complémentaires sur le processus d’installation, vous pouvez utiliser le paramètre **-v**  (mode verbeux) et **-h** (permettant d’afficher une barre de progression pendant l’installation). 

<br/>

Tous les paramètres peuvent être combinés : « rpm -i -v -h » est identique à « rpm -ivh ».

<br/>

3. Téléchargez le paquet **Openshot** grâce à la commande suivante : `wget https://li.nux.ro/download/nux/dextop/el7/x86_64/openshot-1.4.3-1.el7.nux.noarch.rpm`
4. Installez le paquet avec `rpm` en utilisant les options **-i**, **-v** et **-h**.

<details><summary> Montrer la solution </summary>
<p>
`rpm -ivh openshot-1.4.3-1.el7.nux.noarch.rpm`
</p>
</details>

<br/>

**Q2: Est-ce que le paquet a été correctement installé ?**

[ ] Oui

[ ] Non

<br/>

**Q3: Si non, pour quelles raisons à votre avis ?**

[ ] Pas de connectivité à Internet

[ ] Problèmes de dépendance de paquets

[ ] rpm n'a pas trouvé le paquet "openshot"

[ ] Il est obligatoire d'utiliser la commande "yum" à la place

<details><summary> Montrer la solution </summary>
<p>
[*] Problèmes de dépendance de paquets    
</p>
<p>
Rpm va vérifier si ces dépendances sont installées sur le système, et échouer lorsque ce n’est pas le cas.
</p>
</details>

<br/>

---

# Exercice 2

Dans ce second exercice, nous allons utiliser la commande `rpm` pour désinstaller des paquets.

<br/>

## Commandes pour désinstaller un paquet

L'opération classique consistant à désinstaller un paquet peut se faire grâce à la commande : `rpm -e PACKAGENAME`

<br/>

Sur la machine où vous vous trouvez actuellement, la commande `cowsay` est installée et fonctionnelle.
Vous pouvez vérifier en exécutant la commande suivante :
`cowsay Hello World!`

<br/>

5. Maintenant désinstallez le paquet grâce à la commande `rpm`

<details><summary> Montrer la solution </summary>
<p>
rpm -e cowsay
</p>
</details>

<br/>

Vous pouvez vérifier que la commande `cowsay` a bien été désinstallée en réexécutant la commande suivante :
`cowsay Hello World!`

<br/>

## Attention aux problèmes de dépendances

Lorsque vous désinstallez un paquet grâce à la commande `rpm`, une vérification des dépendances est également effectuée. Ainsi, un paquet ne peut pas être désinstallé tant que d’autres paquets qui dépendent de celui-ci sont toujours présents sur la machine.

<br/>

6. Par exemple, essayez de désinstaller le paquet **unzip** et observez la réponse :

<details><summary> Montrer la solution </summary>
<p>
rpm -e unzip
</p>
</details>

<br/>

On voit ainsi que le paquet **file-roller-3.28.1-2.el7.x86_64** dépend de **unzip** pour fonctionner.

<br/>

*Remarque : Lorsqu’un paquet est désinstallé, les fichiers de configuration qu’il utilisait sont conservés sur le système. Si vous voulez retirer tout ce qui est associé avec le paquet, il faut alors utiliser l’option -P (pour purge) à la place de -r.*

<br/>

---

# Exercice 3

Dans ce troisième exercice, nous allons utiliser la commande `rpm` pour obtenir des informations complémentaires sur les paquets.

<br/>

## Commande générale et introduction

Pour obtenir des informations complémentaires sur un paquet, vous pouvez utiliser l'option **-qi** de la commande `rpm`.

<br/>

Prenons par exemple le paquet : **cowsay** que vous pouvez à nouveau installer grâce à `rpm -i cowsay-3.04-4.el7.noarch.rpm`.

<br/>

7. Réinstallez le paquet **cowsay**

<br/>

8. Utilisez la commande `rpm` avec l'option `-qi` sur le paquet **cowsay** et observez le résultat obtenu.

<details><summary> Montrer la solution </summary>
<p>
rpm -qi cowsay
</p>
</details>

<br/>

Maintenant que vous avez exécuté la commande correspondante, répondez aux questions ci-dessous :

**Q1: Sur quel type d'architecture fonctionne ce paquet ?**

[ ] x86

[ ] noarch

[ ] libvirt

[ ] 32 bits

<details><summary> Montrer la solution </summary>
<p>
Architecture: noarch
</p>
</details>
<br/>

**Q2: Quel est le nom du mainteneur du paquet ?**

[ ] Cowsay

[ ] Debian Multimedia Maintainers

[ ] RedHat Linux Enterprise

[ ] Fedora Project

<details><summary> Montrer la solution </summary>
<p>
Packager    : Fedora Project
</p>
</details>
<br/>

## Vérifier l'intégrité d'un paquet

Il est possible de vérifier l'intégrité d'un paquet grâce à RPM, pour s'assurer qu'il n'a pas été modifié, altéré, ou vérolé, grâce à l'option **-V** de la commande **rpm**.

<br/>

Prenons par exemple le paquet : **cowsay** dont vous pouvez vérifier l'intégrité grâce à `rpm -V cowsay-3.04-4.el7.noarch.rpm`.

<br/>

9. Vérifiez l'intégrité du paquet **cowsay**

**A connaître pour l'examen**

<br/>

---

# Exercice 4

Dans ce quatrième exercice, nous allons utiliser la commande `rpm` pour lister les paquets installés sur le système et leur contenu.

<br/>

## Commande générale et introduction

Pour obtenir une liste de tous les paquets installés sur votre système, vous pouvez utiliser l'option **-qa** de la commande `rpm`

10. Utilisez la commande et répondez aux questions suivantes:

<details><summary> Montrer la solution </summary>
<p>
`rpm -qa`
</p>
</details>

<br/>

**Q1: Parmi les paquets suivants, lesquels sont installés sur le système ?**

[ ] udev

[ ] yarn

[ ] httpd

[ ] haproxy

<details><summary> Montrer la solution </summary>
<p>
rpm -qa | grep curl
    </p>
    <p>
rpm -qa | grep udev        
    </p>
    <p>
rpm -qa | grep yarn
    </p>
<p>
rpm -qa | grep httpd
    </p>
    <p>
rpm -qa | grep haproxy
</p>
</details>

<br/>

## Les fichiers installés par un paquet spécifique

Pour obtenir la liste de tous les fichiers installés par un paquet spécifique, vous pouvez utiliser l'option **-ql** de la commande `rpm`.

<br/>

**Q2: Parmi les fichiers suivants, lesquels sont installés sur le système par le paquet "curl" ?**

[ ] /usr/bin/curl

[ ] /usr/share/man/man1/curl.1.gz

[ ] /lib/curl

[ ] /lib/curl/collect

<details><summary> Montrer la solution </summary>
<p>
rpm -ql curl
</p>
</details>

<br/>

Parfois, vous pouvez avoir besoin de trouver le paquet à l'origine d'un fichier en particulier. Pour cela, utilisez l'option **-qf** de la commande `rpm` suivie du nom du fichier.

<br/>

**Q3: Quel paquet est à l'origine du fichier "/usr/bin/unzip"**
<details><summary> Montrer la solution </summary>
<p>
rpm -qf /usr/bin/unzip
</p>
</details>

<br/>

Lorsque vous souhaitez obtenir des informations sur les fichiers d'un paquet qui n'a pas encore été installé, vous avez à ajouter l'option **-p** à **qi** suivie du nom du paquet. <br/>

Par exemple : `rpm -qip openshot-1.4.3-1.el7.nux.noarch.rpm`

